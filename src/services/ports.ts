import http from "./http";
const URL_BASE = "https://cargofive-proxy.herokuapp.com/api";

const portService = {
  getPorts(): Promise<any> {
    return http.get(`${URL_BASE}/ports`);
  },
  getPortsFromLink(link: string): Promise<any> {
    const path = link.replace(/http.*\/api\//, "")
    return http.get(`${URL_BASE}/${path}`);
  },
};

export default portService;
