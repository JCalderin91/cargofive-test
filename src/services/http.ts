const http = {
  get(url: string): Promise<any> {
    return fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        if (!response.ok) throw Error(String(response.status));
        return response.json();
      })
      .catch((error) => {
        throw error;
      });
  },
  post(url: string, payload: object): Promise<any> {
    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    })
      .then((response) => {
        if (!response.ok) throw Error(String(response.status));
        return response.json();
      })
      .catch((error) => {
        throw error;
      });
  },
  put(url: string, payload: object): Promise<any> {
    return fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    })
      .then((response) => {
        if (!response.ok) throw Error(String(response.status));
        return response.json();
      })
      .catch((error) => {
        throw error;
      });
  },
  delete(url: string): Promise<any> {
    return fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        if (!response.ok) throw Error(String(response.status));
        return response.json();
      })
      .catch((error) => {
        throw error;
      });
  },
};

export default http;
