interface Port {
  id: number;
  name: string;
  country: string;
  continent: string;
  coordinates: string;
  continentFullNane?: any;
}

export default Port;
