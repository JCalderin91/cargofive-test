import { ref, computed } from "vue";
import Port from "@/interfaces/Port";
import portService from "@/services/ports";

const getSrc = (name: string) => {
  const path = `../assets/icons/continents/${name}.png`;
  const modules = import.meta.globEager("../assets/icons/continents/*.png");
  return modules[path].default;
};

const getFullName = (continentId: string) =>
  continents.find((continent) => continent.id === continentId)?.name;

const validateCoordinates = (coordinates: string) =>
  /^([-+]?)([\d]{1,2})(((\.)(\d+)))(\s*)(([-+]?)([\d]{1,3})((\.)(\d+))?)$/.test(
    coordinates
  );

const mapPorts = (ports: Port[]) =>
  ports.map((port: Port) => ({
    ...port,
    continentFullNane: getFullName(port.continent),
    coordinates: validateCoordinates(port.coordinates)
      ? port.coordinates
      : "N/A",
  }));

const continents = [
  { id: "AF", name: "Africa", path: getSrc("africa") },
  { id: "AS", name: "Asia", path: getSrc("asia") },
  { id: "EU", name: "Europe", path: getSrc("europe") },
  { id: "NA", name: "North America", path: getSrc("north-america") },
  { id: "SA", name: "South America", path: getSrc("south-america") },
  { id: "OC", name: "Oceania", path: getSrc("oceania") },
];

const search = ref<string>("");
const isLoading = ref<boolean>(false);

const selectedContinents = ref<string[]>(["AF", "AS", "EU", "NA", "SA", "OC"]);
const ports = ref<Port[]>([]);
const pagination = ref({
  from: 0,
  to: 0,
  total: 0,
});
const links = ref({
  first: "",
  prev: "",
  next: "",
  last: "",
});

const setResponse = (response: any) => {
  pagination.value = response.meta;
  links.value = response.links;
  ports.value = mapPorts(response.data);
};

function fetchPorts() {
  isLoading.value = true;
  portService
    .getPorts()
    .then((response) => {
      setResponse(response);
    })
    .finally(() => {
      isLoading.value = false;
    });
}

function getPorts(url: string) {
  if (!url) return false;
  isLoading.value = true;
  portService
    .getPortsFromLink(url)
    .then((response) => {
      setResponse(response);
    })
    .finally(() => {
      isLoading.value = false;
    });
}

const portsFiltered = computed(() => {
  if (search.value.length)
    return ports.value.filter((port: Port) => {
      return (
        port.id.toString().toUpperCase().includes(search.value.toUpperCase()) ||
        port.name.toUpperCase().includes(search.value.toUpperCase()) ||
        port.country.toUpperCase().includes(search.value.toUpperCase()) ||
        port.continentFullNane
          .toUpperCase()
          .includes(search.value.toUpperCase())
      );
    });
  else return ports.value;
});

const portsFilterByContinent = computed(() => {
  return portsFiltered.value.filter(({ continent }) =>
    selectedContinents.value.includes(continent)
  );
});

const pickContinent = (continentId: string) => {
  if (continentIsSelected(continentId))
    selectedContinents.value = selectedContinents.value.filter(
      (id) => id !== continentId
    );
  else selectedContinents.value.push(continentId);
};

const continentIsSelected = (continentId: string) =>
  selectedContinents.value.includes(continentId);

const selectedContinentsCount = computed(
  (): number => selectedContinents.value.length
);

export {
  ports,
  search,
  selectedContinents,
  fetchPorts,
  getPorts,
  isLoading,
  pickContinent,
  continentIsSelected,
  continents,
  portsFilterByContinent,
  pagination,
  links,
  selectedContinentsCount,
};
