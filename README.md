# Cargofive - List of ports

### Desarrollo de interfaz para mostrar lista de puertos mediante una tabla.

#### Diseño de vistas en Figma

Inicialmente se realizó el diseño de esta interfaz mediante la herramienta [Figma](figma.com), tomando en consideración algunos estilos aplicados en la pagina web de [Cargofive](cargofive.com) como por ejemplo: colores, tipografía y logo. En este diseño se tomó en cuenta pantallas de escritorio y pantallas de dispositivos móviles.

Enlace: [cargofive-design](https://www.figma.com/file/fq1fn0qjxHRsRca2EchPrW/Cargofive?node-id=8%3A144)

#### Proceso de maquetado de diseño

En esta etapa se inició con la creación de un proyecto base, utilizando la herramienta [Vite](https://vitejs.dev/) y el framework de javascript [VueJs](https://vuejs.org/), para luego iniciar con el maquetado del diseño realizado en Figma.

Para la organización de los componentes visuales se usó la metodología [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/).

Para la organización y mayor velocidad al momento de crear los estilos visuales se usó [SCSS](https://sass-lang.com/) que luego es procesado y convertido a CSS.

#### Conexión a la api proporcionada
Para realizar las peticiones se uso la API [Fetch](https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Using_Fetch) (Nativa)

#### Despliegue al servidor

El despliegue fue realizado utilizando [Vercel](vercel.com) como plataforma para alojamiento de archivos estaticos y de proyectos desarrollados con frameworks de frontend.

···

Luego del despliegue exitoso de la interfaz desarrollada, surgió un problema para consultar la API proporcionada, ya que esta no contaba con un certificado SSL y esto generaba un error de CORS por acceso a [contenido mixto](https://developer.mozilla.org/en-US/docs/Web/Security/Mixed_content).

Para solucionar esto se realizó un servidor proxy utilizando NodeJs y desplegandolo en Heroku. De esta forma la peticiones realizadas desde la web estan dirigidas al proxy y éste las redirige a la api proporcionada.



### Enlace al proyecto desplegado en Vercel: [Lista de puertos](https://cargofive-test.vercel.app/)

#### Despliegue en local

```
    npm install
    npm run dev
```


#### Herramientas y lenguajes utilizados

![image](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)
![image](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![image](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)
![image](https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D)
![image](https://img.shields.io/badge/Figma-F24E1E?style=for-the-badge&logo=figma&logoColor=white)
![image](https://img.shields.io/badge/Vite-B73BFE?style=for-the-badge&logo=vite&logoColor=FFD62E)
![image](https://img.shields.io/badge/Vercel-000000?style=for-the-badge&logo=vercel&logoColor=white)
![image](https://img.shields.io/badge/Heroku-430098?style=for-the-badge&logo=heroku&logoColor=white)
![image](https://img.shields.io/badge/Node.js-339933?style=for-the-badge&logo=nodedotjs&logoColor=white)
![image](https://img.shields.io/badge/npm-CB3837?style=for-the-badge&logo=npm&logoColor=white)
